﻿using System;
using DeckBuilder.Util;
using Xunit;

namespace DeckBuilder.Test.Util
{
    public class ValidateTest
    {
        public class NotNull
        {
            [Fact]
            public void Should_Throw_ArgumentNullException_If_Argument_Is_Null()
            {
                var ex = Record.Exception(() => Validate.NotNull<object>(null));

                Assert.IsType<ArgumentNullException>(ex);
            }

            [Fact]
            public void Should_Return_Argument_If_Not_Null()
            {
                var test = "Test";
                var result = Validate.NotNull(test);

                Assert.Equal(test, result);
            }
        }

        public class NotNullOrEmpty
        {
            [Theory]
            [InlineData(null)]
            [InlineData("")]
            public void Should_Throw_ArgumentNullException_If_Null_Or_Empty(string data)
            {
                var ex = Record.Exception(() => Validate.NotNullOrEmpty(data));

                Assert.IsType<ArgumentException>(ex);
            }

            [Fact]
            public void Should_Return_Original_Value_If_Not_Null_Or_Empty()
            {
                var test = "Test";
                var result = Validate.NotNullOrEmpty(test);

                Assert.Equal(test, result);
            }
        }
    }
}
