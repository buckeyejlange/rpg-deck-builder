﻿using System;
using System.Threading.Tasks;
using DeckBuilder.Core;
using DeckBuilder.Core.Decks;
using Moq;
using Xunit;

namespace DeckBuilder.Test.Core
{
    public class ExtensionsTest
    {
        public class ValidateOwnershipAsync
        {
            [Fact]
            public async Task Should_Throw_Argument_Exception_If_Not_Owned()
            {
                var repo = new Mock<IOwnerRepository<Deck, Guid>>();
                repo.Setup(r => r.IsOwnedByAsync(It.IsAny<Guid>(), "TestOwner")).ReturnsAsync(false);

                var ex = await Record.ExceptionAsync(async () => await Extensions.ValidateOwnershipAsync(repo.Object, Guid.NewGuid(), "TestOwner"));

                Assert.IsType<ArgumentException>(ex);
            }

            [Fact]
            public async Task Should_Do_Nothing_If_Owned()
            {
                var repo = new Mock<IOwnerRepository<Deck, Guid>>();
                repo.Setup(r => r.IsOwnedByAsync(It.IsAny<Guid>(), "TestOwner")).ReturnsAsync(true);

                await Extensions.ValidateOwnershipAsync(repo.Object, Guid.NewGuid(), "TestOwner");
            }
        }
    }
}
