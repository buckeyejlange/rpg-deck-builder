﻿using System;
using System.Threading.Tasks;
using DeckBuilder.Core;
using DeckBuilder.Core.Decks;
using Moq;
using Xunit;

namespace DeckBuilder.Test.Core
{
    public class CardServiceTest
    {
        public static TheoryData<Card> InvalidCards => new TheoryData<Card>
        {
            null,
            new Card { Name = null, Deck = new Deck { Id = Guid.NewGuid(), Owner = "TestOwner" } },
            new Card { Name = "", Deck = new Deck { Id = Guid.NewGuid(), Owner = "TestOwner" } },
            new Card { Name = "Test", Deck = null },
            new Card { Name = "Test", Deck = new Deck { Id = Guid.NewGuid(), Owner = "TestOwner" }, Attack = -1 },
            new Card { Name = "Test", Deck = new Deck { Id = Guid.NewGuid(), Owner = "TestOwner" }, Defense = -1 },
        };

        public static TheoryData<string> InvalidOwners => new TheoryData<string>
        {
            null,
            ""
        };

        public class AddAsync
        {
            [Theory]
            [MemberData(nameof(InvalidCards), MemberType = typeof(CardServiceTest))]
            public async Task Should_Not_Add_Invalid_Cards(Card card)
            {
                var cardRepo = new Mock<ICardRepository>();
                cardRepo.Setup(r => r.AddAsync(It.IsAny<Card>())).Verifiable();

                var deckRepo = new Mock<IOwnerRepository<Deck, Guid>>();
                deckRepo.Setup(r => r.IsOwnedByAsync(It.IsAny<Guid>(), It.IsAny<string>())).ReturnsAsync(true);

                var service = new CardService(cardRepo.Object, deckRepo.Object);

                var ex = await Record.ExceptionAsync(async () => await service.AddAsync(card));

                Assert.IsAssignableFrom<Exception>(ex);
                cardRepo.Verify(r => r.AddAsync(It.IsAny<Card>()), Times.Never);
            }

            [Fact]
            public async Task Should_Not_Add_Card_If_Deck_Owners_Do_Not_Match()
            {
                var card = new Card { Name = "Test", Deck = new Deck { Id = Guid.NewGuid(), Owner = "BadOwner" } };
                var cardRepo = new Mock<ICardRepository>();
                cardRepo.Setup(r => r.AddAsync(It.IsAny<Card>())).Verifiable();

                var deckRepo = new Mock<IOwnerRepository<Deck, Guid>>();
                deckRepo.Setup(r => r.IsOwnedByAsync(It.IsAny<Guid>(), "BadOwner")).ReturnsAsync(false);

                var service = new CardService(cardRepo.Object, deckRepo.Object);

                var ex = await Record.ExceptionAsync(async () => await service.AddAsync(card));

                Assert.IsAssignableFrom<Exception>(ex);
                cardRepo.Verify(r => r.AddAsync(It.IsAny<Card>()), Times.Never);
            }

            [Fact]
            public async Task Should_Add_Valid_Card()
            {
                var card = new Card { Name = "Test", Deck = new Deck { Id = Guid.NewGuid(), Owner = "TestOwner" } };
                var cardRepo = new Mock<ICardRepository>();
                cardRepo.Setup(r => r.AddAsync(It.IsAny<Card>())).ReturnsAsync(card).Verifiable();

                var deckRepo = new Mock<IOwnerRepository<Deck, Guid>>();
                deckRepo.Setup(r => r.IsOwnedByAsync(It.IsAny<Guid>(), "TestOwner")).ReturnsAsync(true);

                var service = new CardService(cardRepo.Object, deckRepo.Object);

                await service.AddAsync(card);
                
                cardRepo.Verify();
            }
        }

        public class DeleteAsync
        {
            [Theory]
            [MemberData(nameof(InvalidOwners), MemberType = typeof(CardServiceTest))]
            public async Task Should_Not_Delete_Card_If_Owner_Is_Invalid(string owner)
            {
                var cardToDelete = new Card
                {
                    Id = Guid.NewGuid(),
                    Deck = new Deck
                    {
                        Id = Guid.NewGuid(),
                        Owner = "TestOwner"
                    }
                };
                var cardRepo = new Mock<ICardRepository>();
                cardRepo.Setup(r => r.GetByIdAsync(cardToDelete.Id)).ReturnsAsync(cardToDelete);
                cardRepo.Setup(r => r.DeleteAsync(cardToDelete.Id)).Returns(Task.CompletedTask).Verifiable();

                var deckRepo = new Mock<IOwnerRepository<Deck, Guid>>();
                deckRepo.Setup(r => r.IsOwnedByAsync(cardToDelete.Deck.Id, It.IsAny<string>())).ReturnsAsync(true);

                var service = new CardService(cardRepo.Object, deckRepo.Object);
                var ex = await Record.ExceptionAsync(async () => await service.DeleteAsync(cardToDelete.Id, owner));

                Assert.IsAssignableFrom<Exception>(ex);
                cardRepo.Verify(r => r.DeleteAsync(cardToDelete.Id), Times.Never);
            }

            [Fact]
            public async Task Should_Not_Delete_From_Deck_With_Different_Owner()
            {
                var cardToDelete = new Card
                {
                    Id = Guid.NewGuid(),
                    Deck = new Deck
                    {
                        Id = Guid.NewGuid(),
                        Owner = "TestOwner"
                    }
                };
                var cardRepo = new Mock<ICardRepository>();
                cardRepo.Setup(r => r.GetByIdAsync(cardToDelete.Id)).ReturnsAsync(cardToDelete);
                cardRepo.Setup(r => r.DeleteAsync(cardToDelete.Id)).Returns(Task.CompletedTask).Verifiable();

                var deckRepo = new Mock<IOwnerRepository<Deck, Guid>>();
                deckRepo.Setup(r => r.IsOwnedByAsync(cardToDelete.Deck.Id, "BadOwner")).ReturnsAsync(false);

                var service = new CardService(cardRepo.Object, deckRepo.Object);
                var ex = await Record.ExceptionAsync(async () => await service.DeleteAsync(cardToDelete.Id, "BadOwner"));

                Assert.IsAssignableFrom<Exception>(ex);
                cardRepo.Verify(r => r.DeleteAsync(cardToDelete.Id), Times.Never);
            }

            [Fact]
            public async Task Should_Delete_Valid_Card()
            {
                var cardToDelete = new Card
                {
                    Id = Guid.NewGuid(),
                    Deck = new Deck
                    {
                        Id = Guid.NewGuid(),
                        Owner = "TestOwner"
                    }
                };
                var cardRepo = new Mock<ICardRepository>();
                cardRepo.Setup(r => r.GetByIdAsync(cardToDelete.Id)).ReturnsAsync(cardToDelete);
                cardRepo.Setup(r => r.DeleteAsync(cardToDelete.Id)).Returns(Task.CompletedTask).Verifiable();

                var deckRepo = new Mock<IOwnerRepository<Deck, Guid>>();
                deckRepo.Setup(r => r.IsOwnedByAsync(cardToDelete.Deck.Id, "TestOwner")).ReturnsAsync(true);

                var service = new CardService(cardRepo.Object, deckRepo.Object);
                await service.DeleteAsync(cardToDelete.Id, "TestOwner");

                cardRepo.Verify();
            }
        }

        public class GetForDeckAsync
        {
            [Theory]
            [MemberData(nameof(InvalidOwners), MemberType = typeof(CardServiceTest))]
            public async Task Should_Not_Retrieve_If_Owner_Is_Invalid(string owner)
            {
                var deckId = Guid.NewGuid();
                var cardsToGet = new[]
                {
                    new Card { Id = Guid.NewGuid() },
                    new Card { Id = Guid.NewGuid() }
                };

                var cardRepo = new Mock<ICardRepository>();
                cardRepo.Setup(r => r.GetCardsForDeckAsync(deckId)).ReturnsAsync(cardsToGet);

                var deckRepo = new Mock<IOwnerRepository<Deck, Guid>>();
                deckRepo.Setup(r => r.IsOwnedByAsync(deckId, It.IsAny<string>())).ReturnsAsync(true);

                var service = new CardService(cardRepo.Object, deckRepo.Object);
                var ex = await Record.ExceptionAsync(async () => await service.GetForDeckAsync(deckId, owner));

                Assert.IsAssignableFrom<Exception>(ex);
                cardRepo.Verify(r => r.GetCardsForDeckAsync(deckId), Times.Never);
            }

            [Fact]
            public async Task Should_Not_Retrieve_If_Owner_Is_Different_From_Provided()
            {
                var deckId = Guid.NewGuid();
                var cardsToGet = new[]
                {
                    new Card { Id = Guid.NewGuid() },
                    new Card { Id = Guid.NewGuid() }
                };

                var cardRepo = new Mock<ICardRepository>();
                cardRepo.Setup(r => r.GetCardsForDeckAsync(deckId)).ReturnsAsync(cardsToGet);

                var deckRepo = new Mock<IOwnerRepository<Deck, Guid>>();
                deckRepo.Setup(r => r.IsOwnedByAsync(deckId, "BadOwner")).ReturnsAsync(false);

                var service = new CardService(cardRepo.Object, deckRepo.Object);
                var ex = await Record.ExceptionAsync(async () => await service.GetForDeckAsync(deckId, "BadOwner"));

                Assert.IsAssignableFrom<Exception>(ex);
                cardRepo.Verify(r => r.GetCardsForDeckAsync(deckId), Times.Never);
            }

            [Fact]
            public async Task Should_Retrieve_Cards_For_Deck()
            {
                var deckId = Guid.NewGuid();
                var cardsToGet = new[]
                {
                    new Card { Id = Guid.NewGuid() },
                    new Card { Id = Guid.NewGuid() }
                };

                var cardRepo = new Mock<ICardRepository>();
                cardRepo.Setup(r => r.GetCardsForDeckAsync(deckId)).ReturnsAsync(cardsToGet);

                var deckRepo = new Mock<IOwnerRepository<Deck, Guid>>();
                deckRepo.Setup(r => r.IsOwnedByAsync(deckId, "TestOwner")).ReturnsAsync(true);

                var service = new CardService(cardRepo.Object, deckRepo.Object);
                var cards = await service.GetForDeckAsync(deckId, "TestOwner");

                Assert.Equal(cardsToGet, cards);
                cardRepo.Verify();
            }
        }

        public class UpdateAsync
        {
            [Theory]
            [MemberData(nameof(InvalidCards), MemberType = typeof(CardServiceTest))]
            public async Task Should_Not_Update_Invalid_Cards(Card card)
            {
                var cardRepo = new Mock<ICardRepository>();
                cardRepo.Setup(r => r.AddAsync(It.IsAny<Card>())).Verifiable();

                var deckRepo = new Mock<IOwnerRepository<Deck, Guid>>();
                deckRepo.Setup(r => r.IsOwnedByAsync(It.IsAny<Guid>(), It.IsAny<string>())).ReturnsAsync(true);

                var service = new CardService(cardRepo.Object, deckRepo.Object);

                var ex = await Record.ExceptionAsync(async () => await service.AddAsync(card));

                Assert.IsAssignableFrom<Exception>(ex);
                cardRepo.Verify(r => r.AddAsync(It.IsAny<Card>()), Times.Never);
            }

            [Fact]
            public async Task Should_Not_Update_Card_If_Deck_Owners_Do_Not_Match()
            {
                var card = new Card { Id = Guid.NewGuid(), Name = "Test", Deck = new Deck { Id = Guid.NewGuid(), Owner = "BadOwner" } };
                var cardRepo = new Mock<ICardRepository>();
                cardRepo.Setup(r => r.UpdateAsync(It.IsAny<Card>())).Verifiable();

                var deckRepo = new Mock<IOwnerRepository<Deck, Guid>>();
                deckRepo.Setup(r => r.IsOwnedByAsync(It.IsAny<Guid>(), "BadOwner")).ReturnsAsync(false);

                var service = new CardService(cardRepo.Object, deckRepo.Object);

                var ex = await Record.ExceptionAsync(async () => await service.UpdateAsync(card));

                Assert.IsAssignableFrom<Exception>(ex);
                cardRepo.Verify(r => r.UpdateAsync(It.IsAny<Card>()), Times.Never);
            }

            [Fact]
            public async Task Should_Update_Valid_Card()
            {
                var card = new Card { Id = Guid.NewGuid(), Name = "Test", Deck = new Deck { Id = Guid.NewGuid(), Owner = "TestOwner" } };
                var cardRepo = new Mock<ICardRepository>();
                cardRepo.Setup(r => r.UpdateAsync(card)).ReturnsAsync(card).Verifiable();

                var deckRepo = new Mock<IOwnerRepository<Deck, Guid>>();
                deckRepo.Setup(r => r.IsOwnedByAsync(It.IsAny<Guid>(), "TestOwner")).ReturnsAsync(true);

                var service = new CardService(cardRepo.Object, deckRepo.Object);

                await service.UpdateAsync(card);

                cardRepo.Verify();
            }
        }
    }
}
