﻿using System;
using System.Threading.Tasks;
using DeckBuilder.Core;
using DeckBuilder.Core.Decks;
using Moq;
using Xunit;

namespace DeckBuilder.Test.Core
{
    public class DeckServiceTest
    {
        public static TheoryData<Deck> InvalidDecks => new TheoryData<Deck>
        {
            null,
            new Deck { Name = null, Owner = "TestOwner" },
            new Deck { Name = "Test", Owner = null },
            new Deck { Name = "Test", Owner = string.Empty }
        };

        public class AddAsync
        {
            [Theory]
            [MemberData(nameof(InvalidDecks), MemberType = typeof(DeckServiceTest))]
            public async Task Should_Not_Add_Invalid_Decks(Deck deck)
            {
                var repo = new Mock<IOwnerRepository<Deck, Guid>>();
                repo.Setup(r => r.AddAsync(It.IsAny<Deck>())).Verifiable();

                var service = new DeckService(repo.Object);

                var ex = await Record.ExceptionAsync(async () => await service.AddAsync(deck));

                Assert.IsAssignableFrom<Exception>(ex);
                repo.Verify(r => r.AddAsync(It.IsAny<Deck>()), Times.Never);
            }

            [Fact]
            public async Task Should_Add_Valid_Deck()
            {
                var deckToAdd = new Deck
                {
                    Name = "Test",
                    Owner = "TestOwner"
                };

                var repo = new Mock<IOwnerRepository<Deck, Guid>>();
                repo.Setup(r => r.AddAsync(It.IsAny<Deck>())).ReturnsAsync(new Deck
                {
                    Id = Guid.NewGuid(),
                    Name = "Test",
                    Owner = "TestOwner"
                }).Verifiable();

                var service = new DeckService(repo.Object);

                var deck = await service.AddAsync(deckToAdd);

                repo.Verify();
            }

            [Fact]
            public async Task Should_Return_Added_Deck()
            {
                var newId = Guid.NewGuid();
                var deckToAdd = new Deck
                {
                    Name = "Test",
                    Owner = "TestOwner"
                };

                var repo = new Mock<IOwnerRepository<Deck, Guid>>();
                repo.Setup(r => r.AddAsync(It.IsAny<Deck>())).ReturnsAsync(new Deck
                {
                    Id = newId,
                    Name = "Test",
                    Owner = "TestOwner"
                });

                var service = new DeckService(repo.Object);

                var deck = await service.AddAsync(deckToAdd);

                Assert.Equal(newId, deck.Id);
            }
        }

        public class DeleteAsync
        {
            [Fact]
            public async Task Should_Not_Delete_When_Owners_Do_Not_Match()
            {
                var idToDelete = Guid.NewGuid();

                var repo = new Mock<IOwnerRepository<Deck, Guid>>();
                repo.Setup(r => r.DeleteAsync(idToDelete)).Returns(Task.CompletedTask).Verifiable();
                repo.Setup(r => r.IsOwnedByAsync(idToDelete, "TestOwner")).ReturnsAsync(false);

                var service = new DeckService(repo.Object);

                var ex = await Record.ExceptionAsync(async () => await service.DeleteAsync(idToDelete, "TestOwner"));

                Assert.IsType<ArgumentException>(ex);
                repo.Verify(r => r.DeleteAsync(idToDelete), Times.Never);
            }

            [Fact]
            public async Task Should_Delete_With_Valid_Owner()
            {
                var idToDelete = Guid.NewGuid();

                var repo = new Mock<IOwnerRepository<Deck, Guid>>();
                repo.Setup(r => r.DeleteAsync(idToDelete)).Returns(Task.CompletedTask).Verifiable();
                repo.Setup(r => r.IsOwnedByAsync(idToDelete, "TestOwner")).ReturnsAsync(true);

                var service = new DeckService(repo.Object);

                await service.DeleteAsync(idToDelete, "TestOwner");

                repo.Verify();
            }
        }

        public class GetForUserAsync
        {
            [Theory]
            [InlineData(null)]
            [InlineData("")]
            public async Task Should_Not_Allow_Invalid_UserIds(string userId)
            {
                var repo = new Mock<IOwnerRepository<Deck, Guid>>();
                repo.Setup(r => r.GetForUserAsync(It.IsAny<string>())).ReturnsAsync(new Deck[0]);

                var service = new DeckService(repo.Object);

                var ex = await Record.ExceptionAsync(async () => await service.GetForUserAsync(userId));

                Assert.IsAssignableFrom<Exception>(ex);
                repo.Verify(r => r.GetForUserAsync(It.IsAny<string>()), Times.Never);
            }

            [Fact]
            public async Task Should_Retrieve_Decks_For_User()
            {
                var decksToReturn = new Deck[]
                {
                    new Deck { Id = Guid.NewGuid(), Name = "Test1", Owner = "TestOwner" },
                    new Deck { Id = Guid.NewGuid(), Name = "Test2", Owner = "TestOwner" }
                };

                var userId = "TestOwner";

                var repo = new Mock<IOwnerRepository<Deck, Guid>>();
                repo.Setup(r => r.GetForUserAsync(userId)).ReturnsAsync(decksToReturn);

                var service = new DeckService(repo.Object);

                var returnVal = await service.GetForUserAsync(userId);

                Assert.Equal(decksToReturn, returnVal);
            }
        }

        public class UpdateAsync
        {
            [Theory]
            [MemberData(nameof(InvalidDecks), MemberType = typeof(DeckServiceTest))]
            public async Task Should_Not_Update_Invalid_Decks(Deck deck)
            {
                var repo = new Mock<IOwnerRepository<Deck, Guid>>();
                repo.Setup(r => r.UpdateAsync(It.IsAny<Deck>())).ReturnsAsync(deck).Verifiable();

                var service = new DeckService(repo.Object);

                var ex = await Record.ExceptionAsync(async () => await service.AddAsync(deck));

                Assert.IsAssignableFrom<Exception>(ex);
                repo.Verify(r => r.UpdateAsync(It.IsAny<Deck>()), Times.Never);
            }

            [Fact]
            public async Task Should_Not_Update_Deck_With_Incorrect_Owner()
            {
                var deckToUpdate = new Deck
                {
                    Id = Guid.NewGuid(),
                    Name = "Test",
                    Owner = "TestOwner"

                };
                var repo = new Mock<IOwnerRepository<Deck, Guid>>();
                repo.Setup(r => r.UpdateAsync(It.IsAny<Deck>())).ReturnsAsync(deckToUpdate).Verifiable();
                repo.Setup(r => r.IsOwnedByAsync(deckToUpdate.Id, "AnotherOwner")).ReturnsAsync(false);

                var service = new DeckService(repo.Object);

                var ex = await Record.ExceptionAsync(async () => await service.UpdateAsync(deckToUpdate));

                Assert.IsAssignableFrom<Exception>(ex);
                repo.Verify(r => r.UpdateAsync(It.IsAny<Deck>()), Times.Never);
            }

            [Fact]
            public async Task Should_Update_Valid_Deck()
            {
                var deckToUpdate = new Deck
                {
                    Id = Guid.NewGuid(),
                    Name = "Test",
                    Owner = "TestOwner"

                };
                var repo = new Mock<IOwnerRepository<Deck, Guid>>();
                repo.Setup(r => r.UpdateAsync(It.IsAny<Deck>())).ReturnsAsync(deckToUpdate).Verifiable();
                repo.Setup(r => r.IsOwnedByAsync(deckToUpdate.Id, "TestOwner")).ReturnsAsync(true);

                var service = new DeckService(repo.Object);

                await service.UpdateAsync(deckToUpdate);

                repo.Verify();
            }
        }
    }
}
