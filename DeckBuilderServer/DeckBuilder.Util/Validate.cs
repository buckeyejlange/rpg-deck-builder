﻿using System;

namespace DeckBuilder.Util
{
    public static class Validate
    {
        public static T NotNull<T>(T argument, string argumentName = "argument")
        {
            if (argument == null)
            {
                throw new ArgumentNullException(argumentName);
            }

            return argument;
        }

        public static string NotNullOrEmpty(string argument, string argumentName = "argument")
        {
            if (string.IsNullOrEmpty(argument))
            {
                throw new ArgumentException($"{argumentName} cannot be null or empty.", argumentName);
            }

            return argument;
        }
    }
}
