﻿using System;

namespace DeckBuilder.Core.Decks
{
    public class Card
    {
        public Guid Id { get; set; }

        public Deck Deck { get; set; }

        public string ImageId { get; set; }

        public string Name { get; set; }

        public int Attack { get; set; }

        public int Defense { get; set; }

        // When types are defined, look at limiting this from just a string.
        public string Type { get; set; }
    }
}
