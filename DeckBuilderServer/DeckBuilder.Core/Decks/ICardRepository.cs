﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DeckBuilder.Core.Decks
{
    public interface ICardRepository : IRepository<Card, Guid>
    {
        Task<IEnumerable<Card>> GetCardsForDeckAsync(Guid deckId);
    }
}
