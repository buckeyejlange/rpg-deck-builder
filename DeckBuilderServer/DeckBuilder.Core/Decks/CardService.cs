﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static DeckBuilder.Util.Validate;

namespace DeckBuilder.Core.Decks
{
    public class CardService : ICardService
    {
        private ICardRepository _cardRepo;
        private IOwnerRepository<Deck, Guid> _deckRepo;

        public CardService(ICardRepository cardRepo, IOwnerRepository<Deck, Guid> deckRepo)
        {
            _cardRepo = cardRepo;
            _deckRepo = deckRepo;
        }

        public async Task<Card> AddAsync(Card card)
        {
            await ValidateCardAsync(card);

            return await _cardRepo.AddAsync(card);
        }

        public async Task DeleteAsync(Guid id, string ownerId)
        {
            NotNullOrEmpty(ownerId, nameof(ownerId));

            // Not ideal, but I don't want to add an owner field to card as well. Maybe I should reconsider?
            var card = await _cardRepo.GetByIdAsync(id);
            await _deckRepo.ValidateOwnershipAsync(card.Deck.Id, ownerId);

            await _cardRepo.DeleteAsync(id);
        }

        public async Task<IEnumerable<Card>> GetForDeckAsync(Guid deckId, string ownerId)
        {
            NotNullOrEmpty(ownerId, nameof(ownerId));

            await _deckRepo.ValidateOwnershipAsync(deckId, ownerId);

            return await _cardRepo.GetCardsForDeckAsync(deckId);
        }

        public async Task<Card> UpdateAsync(Card card)
        {
            await ValidateCardAsync(card);

            return await _cardRepo.UpdateAsync(card);
        }

        private async Task ValidateCardAsync(Card card)
        {
            NotNull(card, nameof(card));
            NotNullOrEmpty(card.Name, nameof(card.Name));
            NotNull(card.Deck, nameof(card.Deck));

            if (card.Attack < 0)
            {
                throw new ArgumentException("Attack cannot be less than 0");
            }

            if (card.Defense < 0)
            {
                throw new ArgumentException("Defense cannot be less than 0");
            }

            await _deckRepo.ValidateOwnershipAsync(card.Deck.Id, card.Deck.Owner);
        }
    }
}
