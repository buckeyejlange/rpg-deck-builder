﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DeckBuilder.Core.Decks
{
    public interface ICardService
    {
        Task<Card> AddAsync(Card card);

        Task DeleteAsync(Guid id, string ownerId);

        Task<IEnumerable<Card>> GetForDeckAsync(Guid deckId, string ownerId);

        //Task<Card> GetByIdAsync(Guid id, string ownerId);

        Task<Card> UpdateAsync(Card card);
    }
}
