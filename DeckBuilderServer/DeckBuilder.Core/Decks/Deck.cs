﻿using System;

namespace DeckBuilder.Core.Decks
{
    public class Deck
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        
        public string Owner { get; set; }
    }
}
