﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static DeckBuilder.Util.Validate;

namespace DeckBuilder.Core.Decks
{
    public class DeckService : IDeckService
    {
        private IOwnerRepository<Deck, Guid> _repo;

        public DeckService(IOwnerRepository<Deck, Guid> repo)
        {
            _repo = NotNull(repo, nameof(repo));
        }

        public async Task<Deck> AddAsync(Deck deck)
        {
            ValidateDeck(deck);

            return await _repo.AddAsync(deck);
        }

        public async Task DeleteAsync(Guid id, string userId)
        {
            NotNullOrEmpty(userId);
            
            await _repo.ValidateOwnershipAsync(id, userId);

            await _repo.DeleteAsync(id);
        }

        public async Task<IEnumerable<Deck>> GetForUserAsync(string userId)
        {
            return await _repo.GetForUserAsync(NotNullOrEmpty(userId, nameof(userId)));
        }

        public async Task<Deck> UpdateAsync(Deck deck)
        {
            ValidateDeck(deck);
            await _repo.ValidateOwnershipAsync(deck.Id, deck.Owner);

            return await _repo.UpdateAsync(deck);
        }

        private void ValidateDeck(Deck deck)
        {
            NotNull(deck, nameof(deck));
            NotNull(deck.Name, nameof(deck.Name));
            NotNullOrEmpty(deck.Owner, nameof(deck.Owner));
        }
    }
}
