﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DeckBuilder.Core.Decks
{
    public interface IDeckService
    {
        Task<Deck> AddAsync(Deck deck);
        Task DeleteAsync(Guid id, string userId);
        Task<IEnumerable<Deck>> GetForUserAsync(string userId);
        Task<Deck> UpdateAsync(Deck deck);
    }
}
