﻿using System;
using System.Threading.Tasks;

namespace DeckBuilder.Core
{
    public static class Extensions
    {
        public static async Task ValidateOwnershipAsync<T, TKey>(this IOwnerRepository<T, TKey> repo, TKey id, string owner) where T :class
        {
            if (!(await repo.IsOwnedByAsync(id, owner)))
            {
                throw new ArgumentException("Insufficient ownership");
            }
        }
    }
}
