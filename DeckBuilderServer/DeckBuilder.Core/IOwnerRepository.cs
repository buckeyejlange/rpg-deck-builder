﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DeckBuilder.Core
{
    public interface IOwnerRepository<T, TKey> : IRepository<T, TKey> where T : class
    {
        Task<IEnumerable<T>> GetForUserAsync(string userId);

        Task<bool> IsOwnedByAsync(TKey id, string owner);
    }
}
