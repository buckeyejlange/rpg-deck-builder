﻿using System.Threading.Tasks;

namespace DeckBuilder.Core
{
    public interface IRepository<T, TKey> where T : class
    {
        Task<T> AddAsync(T item);
        Task DeleteAsync(TKey key);
        Task<T> GetByIdAsync(TKey key);
        Task<T> UpdateAsync(T item);
    }
}
