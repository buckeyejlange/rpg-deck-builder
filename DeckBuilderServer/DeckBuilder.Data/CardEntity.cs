﻿using System;

namespace DeckBuilder.Data
{
    public class CardEntity
    {
        public Guid Id { get; set; }

        public DeckEntity Deck { get; set; }

        public Guid DeckId { get; set; }

        public string ImageId { get; set; }

        public string Name { get; set; }

        public int Attack { get; set; }

        public int Defense { get; set; }

        public string Type { get; set; }
    }
}
