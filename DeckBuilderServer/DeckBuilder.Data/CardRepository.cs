﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DeckBuilder.Core.Decks;
using Microsoft.EntityFrameworkCore;

namespace DeckBuilder.Data
{
    public class CardRepository : ICardRepository
    {
        private DbContext _context;
        private IMapper _mapper;

        public CardRepository(DbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Card> AddAsync(Card item)
        {
            var entity = _mapper.Map<CardEntity>(item);
            entity.Id = Guid.NewGuid();
            _context.Set<CardEntity>().Add(entity);

            await _context.SaveChangesAsync();

            return _mapper.Map<Card>(entity);
        }

        public async Task DeleteAsync(Guid key)
        {
            var entity = await _context.Set<CardEntity>().FindAsync(key);
            _context.Set<CardEntity>().Remove(entity);

            await _context.SaveChangesAsync();
        }

        public async Task<Card> GetByIdAsync(Guid key)
        {
            var entity = await _context.Set<CardEntity>().Include(c => c.Deck).FirstAsync(c => c.Id == key);

            return _mapper.Map<Card>(entity);
        }

        public async Task<IEnumerable<Card>> GetCardsForDeckAsync(Guid deckId)
        {
            var deckEntity = await _context.Set<DeckEntity>().Include(d => d.Cards).FirstAsync(d => d.Id == deckId);

            return deckEntity.Cards.Select(c => _mapper.Map<Card>(c));
        }

        public async Task<Card> UpdateAsync(Card item)
        {
            var entity = await _context.Set<CardEntity>().FindAsync(item.Id);
            _mapper.Map(item, entity);

            await _context.SaveChangesAsync();

            return _mapper.Map<Card>(entity);
        }
    }
}
