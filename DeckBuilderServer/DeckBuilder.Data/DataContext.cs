﻿using Microsoft.EntityFrameworkCore;

namespace DeckBuilder.Data
{
    public class DataContext : DbContext
    {
        public DbSet<CardEntity> Cards { get; set; }
        public DbSet<DeckEntity> Decks { get; set; }
        
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DeckEntity>()
                .HasMany(d => d.Cards)
                .WithOne(c => c.Deck);
        }
    }
}
