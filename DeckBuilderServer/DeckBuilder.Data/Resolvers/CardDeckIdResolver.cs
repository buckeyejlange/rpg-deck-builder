﻿using System;
using AutoMapper;
using DeckBuilder.Core.Decks;

namespace DeckBuilder.Data.Resolvers
{
    public class CardDeckIdResolver : IValueResolver<Card, CardEntity, Guid>
    {
        public Guid Resolve(Card source, CardEntity destination, Guid destMember, ResolutionContext context)
        {
            return source.Deck.Id;
        }
    }
}
