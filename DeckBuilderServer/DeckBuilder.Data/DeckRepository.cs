﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DeckBuilder.Core;
using DeckBuilder.Core.Decks;
using Microsoft.EntityFrameworkCore;

namespace DeckBuilder.Data
{
    public class DeckRepository : IOwnerRepository<Deck, Guid>
    {
        private DbContext _context;
        private IMapper _mapper;

        public DeckRepository(DbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Deck> AddAsync(Deck item)
        {
            var entity = _mapper.Map<DeckEntity>(item);
            item.Id = Guid.NewGuid(); 
            _context.Set<DeckEntity>().Add(entity);

            await _context.SaveChangesAsync();

            return _mapper.Map<Deck>(entity);
        }

        public async Task DeleteAsync(Guid key)
        {
            var entity = await _context.Set<DeckEntity>().FindAsync(key);
            _context.Set<DeckEntity>().Remove(entity);

            await _context.SaveChangesAsync();
        }

        public async Task<Deck> GetByIdAsync(Guid key)
        {
            var entity = await _context.Set<DeckEntity>().FindAsync(key);

            return _mapper.Map<Deck>(entity);
        }

        public async Task<IEnumerable<Deck>> GetForUserAsync(string userId)
        {
            var entities = await _context.Set<DeckEntity>().Where(d => d.Owner == userId).ToListAsync();

            return entities.Select(e => _mapper.Map<Deck>(e));
        }

        public async Task<bool> IsOwnedByAsync(Guid id, string owner)
        {
            return await _context.Set<DeckEntity>().AnyAsync(d => d.Id == id && d.Owner == owner);
        }

        public async Task<Deck> UpdateAsync(Deck item)
        {
            var entity = await _context.Set<DeckEntity>().FindAsync(item.Id);
            _mapper.Map(item, entity);

            await _context.SaveChangesAsync();

            return _mapper.Map<Deck>(entity);
        }
    }
}
