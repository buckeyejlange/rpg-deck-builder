﻿using System;
using System.Collections.Generic;

namespace DeckBuilder.Data
{
    public class DeckEntity
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Owner { get; set; }

        public IEnumerable<CardEntity> Cards { get; set; }
    }
}
