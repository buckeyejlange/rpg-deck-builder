﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DeckBuilder.API.DataTransfer;
using DeckBuilder.Core.Decks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DeckBuilder.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DeckController : ControllerBase
    {
        private IDeckService _deckService;
        private IMapper _mapper;

        public DeckController(IDeckService deckService, IMapper mapper)
        {
            _deckService = deckService;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<DeckDto> Add(DeckDto deck)
        {
            var newDeck = await _deckService.AddAsync(MapFromDto(deck));
            return _mapper.Map<DeckDto>(newDeck);
        }

        [HttpDelete("{id}")]
        public async Task Delete(Guid id)
        {
            await _deckService.DeleteAsync(id, GetUserId());
        }

        [HttpGet]
        public async Task<IEnumerable<DeckDto>> Get()
        {
            string userId = GetUserId();

            var decks = await _deckService.GetForUserAsync(userId);

            return decks.Select(d => _mapper.Map<DeckDto>(d));
        }

        [HttpPut("{id}")]
        public async Task Update(Guid id, [FromBody] DeckDto deck)
        {
            var deckToUpdate = MapFromDto(deck);
            deckToUpdate.Id = id;
            await _deckService.UpdateAsync(deckToUpdate);
        }

        private string GetUserId()
        {
            return HttpContext.User.Claims.First(c => c.Type == "uid").Value;
        }

        private Deck MapFromDto(DeckDto dto)
        {
            var deck = _mapper.Map<Deck>(dto);
            deck.Owner = GetUserId();

            return deck;
        }
    }
}