﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DeckBuilder.API.DataTransfer;
using DeckBuilder.Core.Decks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DeckBuilder.API.Controllers
{
    [Route("api/Deck/{deckId}/[controller]")]
    [ApiController]
    [Authorize]
    public class CardController : ControllerBase
    {
        private ICardService _cardService;
        private IMapper _mapper;

        public CardController(ICardService cardService, IMapper mapper)
        {
            _cardService = cardService;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<CardDto> Add(Guid deckId, [FromBody] CardDto card)
        {
            var newCard = await _cardService.AddAsync(MapFromDto(card, deckId));

            return _mapper.Map<CardDto>(newCard);
        }

        [HttpDelete("{id}")]
        public async Task Delete(Guid id)
        {
            string ownerId = GetOwnerId();

            await _cardService.DeleteAsync(id, ownerId);
        }

        [HttpGet]
        public async Task<IEnumerable<CardDto>> Get(Guid deckId)
        {
            string ownerId = GetOwnerId();

            var cards = await _cardService.GetForDeckAsync(deckId, ownerId);

            return cards.Select(c => _mapper.Map<CardDto>(c));
        }

        [HttpPut("{id}")]
        public async Task Update(Guid deckId, Guid id, [FromBody] CardDto card)
        {
            var cardToUpdate = MapFromDto(card, deckId);
            cardToUpdate.Id = id;

            await _cardService.UpdateAsync(cardToUpdate);
        }

        private string GetOwnerId()
        {
            return HttpContext.User.Claims.First(c => c.Type == "uid").Value;
        }

        private Card MapFromDto(CardDto dto, Guid deckId)
        {
            var card = _mapper.Map<Card>(dto);
            card.Deck = new Deck
            {
                Owner = GetOwnerId(),
                Id = deckId
            };

            return card;
        }
    }
}
