﻿using System;
using AutoMapper;
using DeckBuilder.API.DataTransfer;
using DeckBuilder.Core;
using DeckBuilder.Core.Decks;
using DeckBuilder.Data;
using DeckBuilder.Data.Resolvers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Okta.AspNetCore;
using SimpleInjector;
using SimpleInjector.Integration.AspNetCore.Mvc;
using SimpleInjector.Lifestyles;

namespace DeckBuilder.API
{
    public class Startup
    {
        private Container _container = new Container();

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = OktaDefaults.ApiAuthenticationScheme;
                options.DefaultChallengeScheme = OktaDefaults.ApiAuthenticationScheme;
                options.DefaultSignInScheme = OktaDefaults.ApiAuthenticationScheme;
            })
            .AddOktaWebApi(new OktaWebApiOptions
            {
                OktaDomain = Configuration["Okta:Domain"],
                ClientId = Configuration["Okta:ClientId"]
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            IntegrateSimpleInjector(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            InitializeContainer(app);
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseMvc();
        }


        private void IntegrateSimpleInjector(IServiceCollection services)
        {
            _container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddSingleton<IControllerActivator>(
                new SimpleInjectorControllerActivator(_container));
            services.AddSingleton<IViewComponentActivator>(
                new SimpleInjectorViewComponentActivator(_container));

            services.EnableSimpleInjectorCrossWiring(_container);
            services.UseSimpleInjectorAspNetRequestScoping(_container);
        }

        private void InitializeContainer(IApplicationBuilder app)
        {
            // Add application presentation components:
            _container.RegisterMvcControllers(app);
            _container.RegisterMvcViewComponents(app);

            // other stuff
            _container.Register<IOwnerRepository<Deck, Guid>, DeckRepository>();
            _container.Register<ICardRepository, CardRepository>();
            _container.Register<ICardService, CardService>();
            _container.Register<IDeckService, DeckService>();
            _container.Register<DbContext>(() =>
            {
                var builder = new DbContextOptionsBuilder<DataContext>();
                builder.UseNpgsql(Configuration.GetConnectionString("PersistentDb"));

                return new DataContext(builder.Options);
            });
            _container.Register(() => CreateMapper());

            // Allow Simple Injector to resolve services from ASP.NET Core.
            _container.AutoCrossWireAspNetComponents(app);
        }

        private IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Deck, DeckEntity>();
                cfg.CreateMap<DeckEntity, Deck>();
                cfg.CreateMap<Deck, DeckDto>();
                cfg.CreateMap<DeckDto, Deck>();
                cfg.CreateMap<Card, CardEntity>()
                    .ForMember(c => c.Deck, opt => opt.MapFrom<CardDeckResolver>())
                    .ForMember(c => c.DeckId, opt => opt.MapFrom<CardDeckIdResolver>());
                cfg.CreateMap<CardEntity, Card>();
                cfg.CreateMap<Card, CardDto>();
                cfg.CreateMap<CardDto, Card>();
            });

            return config.CreateMapper();
        }
    }
}
