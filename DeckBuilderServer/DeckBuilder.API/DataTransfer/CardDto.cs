﻿using System;

namespace DeckBuilder.API.DataTransfer
{
    public class CardDto
    {
        public Guid Id { get; set; }

        public string ImageId { get; set; }

        public string  Name { get; set; }

        public int Attack { get; set; }

        public int Defense { get; set; }

        public string Type { get; set; }
    }
}
