﻿using System;

namespace DeckBuilder.API.DataTransfer
{
    public class DeckDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
